var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var activitySchema = new Schema({
    activeDuration: { type: Number, required: false },
	activityLevel: { type: [], required: false },
	heartRateZones: { type: [], required: false },
	activityName: { type: String, required: false }, 
	averageHeartRate: { type: Number, required: false },
    calories: { type: Number, required: false },
    description: { type: String, required: false },
    distance: { type: Number, required: false },
    duration: { type: Number, required: false },
	elevationGain: { type: Number, required: false },
    logId: { type: Number, required: false },
	startTime: { type: String, required: false },
	manualValuesSpecified: {
		calories: { type: Boolean, required: false },
		distance: { type: Boolean, required: false },
		steps: { type: Boolean, required: false }
	},
    steps: { type: Number, required: false }
}, { collection: 'activities' });

module.exports = mongoose.model('Activity', activitySchema);