var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var sleepSchema = new Schema({
    dateOfSleep: { type: String, required: true },
    duration: { type: Number, required: false },
    efficiency: { type: Number, required: false },
    endTime: { type: String, required: false },
    startTime: { type: String, required: false },
    isMainSleep: { type: Boolean, required: false },
	levels: { type: [], required: false },
	minutesAfterWakeup: { type: Number, required: false },
	minutesAsleep: { type: Number, required: false },
	minutesAwake: { type: Number, required: false },
	logId: { type: Number, required: true, unique: true }
}, { collection: 'sleeps' });



module.exports = mongoose.model('Sleep', sleepSchema);