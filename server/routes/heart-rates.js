var express = require('express');
var app = express();
var request = require('request');
var dateFormat = require('dateformat');
var moment = require('moment');
var async = require("async");

var Heart = require('../models/heart');

function getHeaders() {
    var token = localStorage.getItem('token');
	return {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + token
	};
}

app.get('/heart/update', (req, res) => {
	
	const headers = getHeaders();
	const base_url = 'https://api.fitbit.com/1/user/' + localStorage.getItem('user_id');

	request({
		url: base_url + '/activities/heart/date/today/1d/1min.json',
		headers: headers
	}, function (error, response) {

		var body = JSON.parse(response.body);
		try {
			if (body['activities-heart'].length > 0) {
				var heart_rates = [];
				/* 
				getRates(body['activities-heart'], heart_rates).then(() => {
					async.eachSeries(heart_rates, function(rate, asyncdone) {
						rate.save(asyncdone);
					}, function(err) {
						if (err) {
							return res.status(400).send(err);
						}
				
						res.status(201);
					});
				}) */
	
			}
		} catch (err) {
			res.status(400).send('No se pudieron actualizar los datos')
		}
	})
})
function getRates(heart_rates, array) {
	let long = heart_rates.length;
	return new Promise((resolve, reject) => {
		heart_rates.forEach((ac, index) => {
			Heart.findOne({logId: ac.logId}, (err, act) => {
				if (!act) {
					array.push();
				}
				if(index == long - 1) resolve();
				if(err) reject(err);
			});
		});
	})
}


app.get('/heart/:fecha/:desde/:hasta', (req, res) => {
	
	const headers = getHeaders();
	const base_url = 'https://api.fitbit.com/1/user/' + localStorage.getItem('user_id');

	request({
		url: base_url + '/activities/heart/date/' + req.params.fecha + '/1d/1sec/time/' + req.params.desde + '/' + req.params.hasta + '.json',
		headers: headers
	}, function (error, response) {

		var body = JSON.parse(response.body);
		try {
			if (body['activities-heart'].length > 0) {
				res.status(200).send(body);
			}
		} catch (err) {
			res.status(400).send('No se pudieron encontrar los datos en ese periodo')
		}
	})
})
module.exports = app;