var express = require('express');
var app = express();
var request = require('request');
var dateFormat = require('dateformat');
var moment = require('moment');
var async = require("async");

var Activity = require('../models/activity');

const base_url = 'https://api.fitbit.com/1/user/' + localStorage.getItem('user_id');

function getHeaders() {
    var token = localStorage.getItem('token');
	return {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + token
	};
}

app.get('/activities/update', (req, res) => {
    
    var end_date = moment();
    
	const headers = getHeaders();

	request({
		url: base_url + '/activities/list.json?beforeDate=' + dateFormat(end_date, 'isoDate') + '&sort=desc&offset=0&limit=30',
		headers: headers
	}, function (error, response) {

		var body = JSON.parse(response.body);

		if (body.activities.length > 0) {
			var activities = [];
			
			getActivities(body.activities, activities).then(() => {
				async.eachSeries(activities, function(activity, asyncdone) {
					activity.save(asyncdone);
				}, function(err) {
					if (err) {
						return res.status(400).json({ ok: false, mensaje: 'Error al guardar actividad', errors: err });
					}
			
					res.status(201).json({ ok: true });
				});
			})

		}
	})
})
function getActivities(activities, array) {
	let long = activities.length;
	return new Promise((resolve, reject) => {
		activities.forEach((ac, index) => {
			Activity.findOne({logId: ac.logId}, (err, act) => {
				if (!act) {
					array.push(new Activity({
						activeDuration: ac.activeDuration,
						activityLevel: ac.activityLevel,
						heartRateZones: ac.heartRateZones,
						activityName: ac.activityName, 
						averageHeartRate: ac.averageHeartRate,
						calories: ac.calories,
						description: ac.description,
						distance: ac.distance,
						duration: ac.duration,
						elevationGain: ac.elevationGain,
						logId: ac.logId,
						startTime: ac.startTime,
						manualValuesSpecified: ac.manualValuesSpecified,
						steps: ac.steps
					}));
				}
				if(index == long - 1) resolve();
				if(err) reject(err);
			});
		});
	})
}

app.get('/activities/getByType/:type', async (req, res) => {
	const actividades = await Activity.find({ activityName: req.params.type });
	
	if(!actividades) res.status(400).send('No se ha encontrado actividad en la fecha seleccionada')

	res.status(200).send(actividades);
})

app.get('/activities/getById/:id', async (req, res) => {
	const actividad = await Activity.findOne({ logId: req.params.id });
	
	if(!actividad) res.status(400).send('No se ha encontrado actividad con el Id indicado')

	res.status(200).send(actividad);
})

app.get('/activities/getMultiById/:id1/:id2', async (req, res) => {
	const actividad1 = await Activity.findOne({ logId: req.params.id1 });
	const actividad2 = await Activity.findOne({ logId: req.params.id2 });
	
	if(!actividad1 && !actividad2) res.status(400).send('No se han encontrado actividades con los Ids indicado')

	res.status(200).send([actividad1, actividad2]);
})

module.exports = app;