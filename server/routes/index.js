const express = require('express');
const app = express();

app.use(require('./auth'));
app.use(require('./activities'));
app.use(require('./sleeps'));
app.use(require('./heart-rates'));
module.exports = app;
