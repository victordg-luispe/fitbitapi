var express = require('express');
var app = express();
var request = require('request');
var dateFormat = require('dateformat');
var moment = require('moment');
var async = require("async");

var Sleep = require('../models/sleep');

const base_url = 'https://api.fitbit.com/1.2/user/' + localStorage.getItem('user_id');

function getHeaders() {
    var token = localStorage.getItem('token');
	return {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + token
	};
}

app.get('/sleep/list', (req, res) => {

    var end_date = new Date();

	const headers = getHeaders();
    
    request({
        url: base_url + '/sleep/list.json?beforeDate=' + dateFormat(end_date, 'isoDate') + '&sort=desc&offset=0&limit=30',
        headers: headers
    }, function (error, response) {
		
		var body = JSON.parse(response.body);

		res.status(200).json({
			ok: true,
			medico: sueñoGuardado
		});
	});
});

app.get('/sleep/getByDate/:date', async (req, res) => {
	const sueño = await Sleep.find({ dateOfSleep: req.params.date });
	
	if(!sueño) res.status(400).send('No se han encontrado sueños en la fecha seleccionada')

	res.status(200).send(sueño);
})

app.get('/sleep/update', (req, res) => {
    
    var end_date = moment();
    var start_date = moment().subtract(2, "month");
    
	const headers = getHeaders();

	request({
		url: base_url + '/sleep/date/' + dateFormat(start_date, 'isoDate') + '/' + dateFormat(end_date, 'isoDate') +'.json',
		headers: headers
	}, function (error, response) {

		var body = JSON.parse(response.body);

		if (body.success == false) return res.send(403);

		if (body.sleep.length > 0) {
			var sleeps = [];
			getSleeps(body.sleep, sleeps).then(() => {
				async.eachSeries(sleeps, function(sleep, asyncdone) {
					sleep.save(asyncdone);
				}, function(err) {
					if (err) {
						return res.status(400).json({ ok: false, mensaje: 'Error al guardar sueño', errors: err });
					}
			
					res.status(201).json({ ok: true });
				});
			});
		}
	})
})
function getSleeps(sleeps, array) {
	let long = sleeps.length;
	return new Promise((resolve, reject) => {
		sleeps.forEach((sl, index) => { 
			Sleep.findOne({logId: sl.logId}, (err, slp) => {
				if (!slp) {
					array.push(new Sleep({
						dateOfSleep: sl.dateOfSleep,
						duration: sl.duration,
						efficiency: sl.efficiency,
						startTime: sl.startTime,
						endTime: sl.endTime,
						isMainSleep: sl.isMainSleep,
						levels: sl.levels,
						minutesAfterWakeup: sl.minutesAfterWakeup,
						minutesAsleep: sl.minutesAsleep,
						minutesAwake: sl.minutesAwake,
						logId: sl.logId
					}));
				}
				if(index == long - 1) resolve();
				if(err) reject(err);
			});
		})
	});
}

module.exports = app;