const jwt = require('jsonwebtoken');

// ==============
// Verifica token
// ==============
let verificaToken = (req, res, next) => {
  let token = req.get('token');

  jwt.verify(token, process.env.SEED, (err, decoded) => {
    if (err) {
      return res.status(401).json({
        ok: false,
        err: {
          message: 'Token no válido'
        }
      });
    }
    req.usuario = decoded.usuario;
    next();
  });
};

// ==================
// Verifica ADMINROLE
// ==================
let verificaAdmin_Role = (req, res, next) => {
  let usuario = req.usuario;

  if (usuario.role === 'ADMIN_ROLE') {
    next();
  } else {
    return res.json({
      ok: false,
      err: {
        message: 'Role no permitido para esta petición'
      }
    });
  }
};

/*
 * Verificar Admin o mismo Usuario
 */
let verificaAdminOmismoUsuario = function(req, res, next) {
  let usuario = req.usuario;
  let id = req.params.id;

  if (usuario.role === 'ADMIN_ROLE' || usuario._id === id) {
    next();
    return;
  } else {
    return res.status(401).json({
      ok: false,
      mensaje: 'Token incorrecto',
      errors: {
        message: 'Role no permitido para esta petición'
      }
    });
  }
};

// =====================
// Verifica Token imagen
// =====================
let verificaTokenImg = (req, res, next) => {
  let token = req.query.token;

  jwt.verify(token, process.env.SEED, (err, decoded) => {
    if (err) {
      return res.status(401).json({
        ok: false,
        err: {
          message: 'Token no válido'
        }
      });
    }
    req.usuario = decoded.usuario;
    next();
  });
};
module.exports = {
  verificaToken,
  verificaAdmin_Role,
  verificaAdminOmismoUsuario,
  verificaTokenImg
};
