require('./config/config');

const express = require('express');
const mongoose = require('mongoose');
const path = require('path');

const app = express();
const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
// parse application/json
app.use(bodyParser.json());
app.use(express.static(path.resolve(__dirname, '../public')));

//CORS
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Token'
  );
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  next();
});

//Configuración global de rutas
app.use(require('./routes/index'));

mongoose.connect(
  process.env.URLDB,
  (err, res) => {
    if (err) throw err;
    console.log(`Base de datos \x1b[32m%s\x1b[0m`, `ONLINE`);
  }
);

app.listen(process.env.PORT, () => {
  console.log(`Escuchando el puerto ${process.env.PORT}: \x1b[32m%s\x1b[0m`, `ONLINE`);
});
