# Backend-Server Fitbit

[![Donate](http://img.shields.io/badge/paypal-donate-179BD7.svg)](https://www.paypal.me/LuisPeToloy) [![MIT license](https://img.shields.io/badge/license-MIT-lightgrey.svg)](http://opensource.org/licenses/MIT)

Si te gustó el proyecto copate y hacé una donación [![Donate](http://img.shields.io/badge/paypal-donate-179BD7.svg)](https://www.paypal.me/LuisPeToloy)

# V0.0.1

## Sólo obtiene el token de Fitbit para consumir la data

## Author

- [LuisPe-Toloy](https://github.com/LuisPe)
